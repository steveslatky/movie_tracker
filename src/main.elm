-- MODEL


module Main exposing (..)

import Date exposing (Date)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode exposing (..)



main =
  Html.program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }



type alias Model =
    { movieList : List Movie
      , newMovie : String
      , likedNewMovie : Bool
      , test : String
    }


type alias Movie =
    { name : String
    , date : Date
    , imdb : String
    , rotten : String
    , liked : Bool }



-- INIT


init : ( Model, Cmd Msg )
init =
    ( Model [] "" False "", Cmd.none )



-- UPDATE


type Msg
    = Add
    | UpdateMovie String
    | ToggleLike
    | NewMovie (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Add ->
            ( model, getMovieDetail model.newMovie )

        UpdateMovie movie ->
            ({model | newMovie = movie}, Cmd.none)

        ToggleLike ->
            ({model | likedNewMovie = not model.likedNewMovie}, Cmd.none)

        NewMovie (Ok movieJson) ->
            ({model | test = movieJson }, Cmd.none)

-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h1 [ ] [text "Movie Tracker"]
        , input  [type_ "text", placeholder "Enter a Movie", onInput UpdateMovie ] []
        , checkbox ToggleLike "Liked it!"
        , button [ onClick Add ] [ text "Add" ]
        , br [] []
        , text model.test
        ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- Empty for now.


addMovie : String -> List Movie
addMovie movie =
    []


checkbox : msg -> String -> Html msg
checkbox msg name =
  label
    [ style [("padding", "10px")]
    ]
    [ input [ type_ "checkbox", onClick msg ] []
    , text name
    ]


-- OMDb API

getMovieDetail : String -> Cmd Msg
getMovieDetail movie =
    let
      url = "http://www.omdbapi.com/?apikey=64fc7c6a&t=" ++ movie
    in
    Http.send NewMovie (Http.get url)